//arrayActivity.js

// Task: Create a Pila Management System with the following functions.
// 
const pila = [];
// 
// 1. showAll
// 	will return an array of those who are in line.

function showAll () {
	return pila;
}
 	
// 	2. addQueue()
// 	accepts a name as a parameter(string), will add the data at the end of the array.

function addQueue (queue) {
	const newQueue = (queue);
	pila.push (newQueue);
	return "Next queue is: " + newQueue;
}

// 	3. deQueue()
// 	NO PARAMETER. removes the first person in the array.

function deQueue () {
	pila.shift ();
	return "Success delete first queue";
}	

// 	4. expedite(index)
// 	It will remove the data based on the given index
// 	

function expedite (index) {
	pila.splice (index, 1);
	return "Success remove queue";
}

// 	5. reverseDequeue()
// 	NO PARAMETER. removes the last person in the array
// 	

function reverseDequeue() {
	pila.pop ();
	return "Success delete last queue";
}

// 	6. addVIP()
// 	accepts a name as a parameter(string),
// 	add the data at the beginning of the array
// 	

function addVIP (vip) {
	const newVIP = (vip);
	pila.unshift (newVIP);
	return "VIP queue is: " + newVIP;
}

// 	7. updateName(index, newName)
// 	update the data based on the given parameters.
// 	

function updateName (index, newName) {
	pila[index]=newName;
	return "Successfully updated queue!";
}