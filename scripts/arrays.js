BDLE
  pwfmalla@gmail.com
WD004-3 Notes Arrays
  Jun 17 2020 20:21:16 PM +08:00

 
 
 

1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
38
39
40
41
42
43
44
45
46
47
48
49
50
51
52
53
54
55
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
80
81
82
83
84
85
86
87
88
89
90
91
92
93
94
95
96
97
98
99
100
101
102
103
104
105
106
107
108
109
// Array --- is a collection of related data. Denoted by [].
//Ex:

const fruits = ['Apple', 'Banana', 'Kiwi'];

const ages = [1,2,3,4];

const students = [ 
	{name: "brandon", age: 11}, 
	{name: "brenda", age: 12},
	{name: "celmer", age: 15},
	{name: "archie", age: 16}
];

//it is recommended to use plural form when naming an array, and singular when naming an object

//to access an array, we need to use bracket notation and the index of the item we want to access.
//
//index is the position of the data relative to the position of the first data.
//index always starts at 0.
//
//To add a data in an array,
//we will use the method .push();
//array.push(dataWeWantToAdd);
//the method will return the length of the array.
//length means # of data inside an array
//The new data will be added at the end.
//
//to update a data, access the data first via bracket notation and then re-assign a value.
//array[index] = newValue;
//
//to delete a data at the end of an array, we use the method pop();
//array.pop();
//It will return the deleted data;
//
//to delete a data at the start of an array, we use the method shift();
//array.shift();
//It will return the deleted data;
//
//To add a data in the beginning/ Start/ umpisa of an array, we use the method unshift(dataWeWantToAdd);
//array.unshift(dataWeWantToAdd);
//This iwll return the new length of the array
//
//push--- add / end
//pop --- delete / end
//shift --- delete / start
//unshift --- add / start
//
//.splice();
//It takes at least 2 arguments.
//array.splice( startingIndex, noOfItemsYouWantToDelete, --optional dataWeWantToInsert );
//This will return an array of deleted items;
//If the deleteCount is 0, it will just insert the new data from the starting index.

Todo list

//Our task.
//Create a todo list with the following functions.
//
//1. showTodos 
//	Will return an array of all todos
//	
//2. addTodos(todo) -- done
//	Will add a new todo in our array of todos
//	
//3. updateTodo()
//	Given an index, will update the todo
//	
//4. deleteTodo()
//	Given an index, will delete a todo
//	

// 1. Create an empty todos array

const todos = [];

// Create a function that will add a new todo.
function addTodo(task){
	//Things to consider:
	//1. What is the data type of the todo data? String? Object?
	//2. What are the parameters that this function needs?
	
	const newTodo = task;

	//push the newTodo into our todos array.
	todos.push(newTodo);

	return "Successfully added " + newTodo;
}

function showTodos(){
	return todos;
}

function updateTodo(index, updatedTask){
	// Things to consider:
	// 1. When updating a data, we need something to identify which data we want to update. In this case, we will use the index.
	// 2. We need the updated value.
	todos[index]= updatedTask;
	return "Successfully updated task!";
}

function deleteTodo(index){
	//Things to consider:
	//1. When deleting a data, we need something to identify which data we want to delete.
	todos.splice(index, 1);
	return "Successfully deleted task!";
}

Bootcamp Dynamic Learning Environment