const toDo = [];	

function addToDo (task) {
	const newToDo = task;
	toDo.push(newToDo);
	return "Successfully added " + newToDo;
}

function showToDos() {
	return toDo;
}

function updateTodo (index, updatedTask) {
	toDo[index]=updatedTask;
	return "Successfully updated task!";
}

function deleteToDo (index) {
	toDo.splice(index, 1);
	return "Successfully deleted task!";
}